import { login } from "@/api/user";

const user = {
  state: {
    user: JSON.parse(window.localStorage.getItem("user")),
    userToken: window.localStorage.getItem("userToken"),
    userLoginTime: window.localStorage.getItem("userLoginTime"),
    isLoginState: false
  },

  mutations: {
    SET_USER: (state, user) => {
      let now = new Date().getTime() + "";
      state.user = user;
      state.userToken = user.userToken;
      state.userLoginTime = now;
      window.localStorage.setItem("user", JSON.stringify(user));
      window.localStorage.setItem("userToken", user.userToken);
      window.localStorage.setItem("userLoginTime", now);
    },
    SET_USER_LOGIN: (state, isLogin) => {
      state.isLoginState = isLogin;
    },
    UPDATE_USER: (state, user) => {
      state.user.nickName = user.nickName;
    },
    LOGOUT_USER: state => {
      state.user = "";
      state.userToken = "";
      state.userLoginTime = "";
      state.isLoginState = false;
      window.localStorage.removeItem("user");
      window.localStorage.removeItem("userToken");
      window.localStorage.removeItem("userLoginTime");
    }
  },

  actions: {
    // 登录
    async login({ commit }, { username, password, success, fail }) {
      login({
        username: username,
        password: password,
        success: res => {
          commit("SET_USER", res.data);
          if (success) {
            success(res);
          }
        },
        fail: fail
      });
    },
    // 登出
    logout({ commit }) {
      commit("LOGOUT_USER");
    },
    updateUser({ commit }, user) {
      commit("SET_USER", user);
    }
  }
};

export default user;
