const getters = {
  user: state => state.user,
  userToken: state => state.user.userToken
};

export default getters;
