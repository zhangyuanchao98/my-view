import Vue from 'vue'
import App from './App.vue'
import router from './router'
import './plugins/iview.js'
import devarticle from './components/dev-article.vue'
import userModal from './components/UserModal.vue'
Vue.config.productionTip = false
Vue.component("dev-article",devarticle);
Vue.component("user-modal",userModal)
new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
