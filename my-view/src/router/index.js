import Vue from 'vue'
import VueRouter from 'vue-router'
import store from "@/store";


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/user/list'
  },
  {    path: '/user/list',
    name: 'UserList',
    component: () => import(/* webpackChunkName: "about" */ '../views/UserList.vue')
  },
  {
    path: '/api/list',
    name: 'UserList',
    component: () => import(/* webpackChunkName: "about" */ '../views/UserList.vue')
  },
  {
    path: '/operation/list',
    name: 'OperationList',
    component: () => import(/* webpackChunkName: "about" */ '../views/operation/OperationList.vue')
  },
  {
    path: '/operation/desc',
    name: 'OperationList',
    component: () => import(/* webpackChunkName: "about" */ '../views/operation/OperationDesc.vue')
  },
  {
    path: '/log/list',
    name: 'LogList',
    component: () => import(/* webpackChunkName: "about" */ '../views/LogList.vue')
  },
  {
    path: '/system/list',
    name: 'SystemList',
    component: () => import(/* webpackChunkName: "about" */ '../views/SystemList.vue')
  },
];

const router = new VueRouter({
  routes
})
router.beforeEach((to, from, next) => {

  let isLoginState = store.state.user.isLoginState;
  if (!isLoginState) {
    let userToken = store.state.user.userToken;
    let userLoginTime = store.state.user.userLoginTime;
    let nowTime = new Date().getTime();
    isLoginState =
        userToken !== undefined &&
        userToken != null &&
        nowTime - parseInt(userLoginTime) < 3 * 24 * 60 * 60 * 1000;
  }
  // 已登录
  if (isLoginState) {
    // 设置vuex登录状态为已登录
    store.commit("SET_USER_LOGIN", true);
    store.state.user.isLoginState = true;

    // 已登录状态定向回首页
      next({ path: "/home" });
  }
  // 未登录
  else {
    // 默认所有页面都需要登录
      next({ path: "/login" });
      this.$Message.info("请先登录");
    }
});
export default router
