# my-view

#### 介绍

使用iview+vue编写的一款layout布局的管理系统
里面使用大量的栅格布局，尽可能少写或不写css样式来完成布局
数据均为模拟数据  后期从服务端获取数据
现有 ：
用户管理(已完成)
运营管理(未完成)
日志管理(未完成)
接口管理(未完成)
系统管理(未完成)

#### 软件架构
vue+vue-router+vuex(未完成)


#### 安装教程

1.  npm intsall
2.  npm run dev
#### 图片示例
![用户管理示例1](https://images.gitee.com/uploads/images/2020/0918/104823_df034f31_5080640.png "示例1.png")
![用户管理示例2](https://images.gitee.com/uploads/images/2020/0918/104854_f3000525_5080640.png "示例2.png")